/**
 * Copyright 2014 http://www.iutenligne.net/
 * Tous droits réservés
 * 
 * 
 * ----------------------------------------------------------------------------
 * Ce fichier fait partie de IEL-tpvirtuel.
 *
 * IEL-tpvirtuel est un logiciel libre ; vous pouvez le redistribuer ou le modifier 
 * suivant les termes de la GNU General Public License telle que publiée par
 * la Free Software Foundation ; soit la version 3 de la licence, soit 
 * (à votre gré) toute version ultérieure.
 * 
 * IEL-tpvirtuel est distribué dans l'espoir qu'il sera utile, 
 * mais SANS AUCUNE GARANTIE ; pas même la garantie implicite de 
 * COMMERCIABILISABILITÉ ni d'ADÉQUATION à UN OBJECTIF PARTICULIER. 
 * Consultez la GNU General Public License pour plus de détails.
 * 
 * Vous devez avoir reçu une copie de la GNU General Public License 
 * en même temps que IEL-tpvirtuel ; si ce n'est pas le cas, 
 * consultez <http://www.gnu.org/licenses>.
 * ----------------------------------------------------------------------------
 * This file is part of IEL-tpvirtuel.
 *
 * IEL-tpvirtuel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IEL-tpvirtuel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with IEL-tpvirtuel.  If not, see <http://www.gnu.org/licenses/>.
 * ----------------------------------------------------------------------------
 */
 
/* 
    
    Created on : 2014
    Author     : Hamza ABED, <b>hamza.abed.professionel@gmail.com</b>, 2014
    Author     : Philippe Pernelle, <b>philippe.pernelle@gmail.com</b>, 2014
*/
 
 var canvasLine=document.getElementById('canvasLine');
      var contextLine=canvasLine.getContext('2d');
      
      var canvas = document.getElementById('myCanvas');
      var context = canvas.getContext('2d');
      

var xpiv1=100; var xpiv2=200; 
var ypiv1= 60; var ypiv2=250;

var ypiv1I=60; ypiv2I=250;


var VeMax=10;
var R1=1;
var R2=1;
function drawByCalc()
{   var v=0;
    var firstBoucle=true;
    var ch="";
    for(i=50;i<250;i++)
    { v=Math.PI*VeMax*Math.sin(2*3.14*i*10+4.7);
      if(!firstBoucle)
        drawLine(i-1,ya,i,v+150,context,"red");
        drawDot(i,v+150,'red') ;
      
       ch+="\n "+(v);
       ya=v+150;
       firstBoucle=false;
    }
 //alert("ch="+ Math.PI);
}
function drawByCalc2()   //dessiner la courbe orange
{   var v=0;
    var vs=0;
    var va=0;
    var firstBoucle=true;;
    var ch="";
    var xa=0; ya=0;
    for(i=50;i<250;i++)
    { v=Math.PI*VeMax*Math.sin(2*3.14*i*10+4.7);
       vs=-v;
       vs*=(R1/R2);
        if(xa!=0) drawLine(xa,ya,i,vs+150,context,"orange");
        drawDot(i,vs+150,'orange');
       xa=i; ya=vs+150;
       if(!firstBoucle)
       drawLine(va+150,150+va,v+150,150+v,contextLine,"orange");
       drawDotF2(v+150,150+v,'orange') ;
       ch+="\n "+(v);
       
       va=v;
       firstBoucle=false;
    }
 //alert("ch="+ Math.PI);
}



function drawCurve(xBegin,yBegin, xPivot, yPivot,xEnd,yEnd)
{
  context.beginPath();
  context.strokeStyle='red';
  context.lineWidth=4;
  context.moveTo(xBegin,yBegin); ///poit de départ
  context.quadraticCurveTo(xPivot,yPivot, xEnd, yEnd);
  context.stroke();
  drawDot(xBegin, yBegin,'black');
  drawDot(xPivot, yPivot,'black');
  drawDot(xEnd, yEnd,'black');
}
function redrawFunction()
{
context.clearRect(0,0,canvas.width,canvas.height);

contextLine.clearRect(0,0,canvasLine.width,canvasLine.height);
//drawCurve(50,150,xpiv1,ypiv1,150,150);
//drawCurve(150,150,xpiv2,ypiv2,250,150);
//drawDot(150, 150,'green'); ///point centre de courbe
//drawLine();
drawByCalc();drawByCalc2();
//drawBox1(); drawBox2();
}

redrawFunction();

function drawDot(x, y,color)
{
  context.beginPath();
  context.fillStyle=color;
  context.arc(x, y, 1, 0, 2 * Math.PI, true);
  context.fill();
}

function drawDotF2(x, y,color)
{
  contextLine.beginPath();
  contextLine.fillStyle=color;
  contextLine.arc(x, y, 1, 0, 2 * Math.PI, true);
  contextLine.fill();
}



function drawLine(x1,y1,x,y,contextDeDessin,color)
{
      contextDeDessin.beginPath();
      contextDeDessin.moveTo(x1, y1);
      contextDeDessin.lineTo(x,y);
      contextDeDessin.lineWidth = 2;
      contextDeDessin.strokeStyle = color;
      contextDeDessin.lineCap = 'butt';
      
      contextDeDessin.stroke();
}


function valueRangeChanged()
{ 
var newValue=document.getElementById("slider1").value;

R1=eval(document.getElementById("slider2").value);
R2=eval(document.getElementById("slider3").value);
VeMax=eval(newValue)*5;

var span = document.getElementById('val1');
var spanR1 = document.getElementById('valR1');
var spanR2 = document.getElementById('valR2');
while( span.firstChild ) span.removeChild( span.firstChild );
while( spanR1.firstChild ) spanR1.removeChild( spanR1.firstChild );
while( spanR2.firstChild ) spanR2.removeChild( spanR2.firstChild );

span.appendChild( document.createTextNode("Ve max = "+newValue+" V") );
spanR1.appendChild( document.createTextNode("R1 = "+R1+"") );
spanR2.appendChild( document.createTextNode("R2 = "+R2+"") );

ypiv1=ypiv1I-newValue*10; ypiv2=ypiv2I+newValue*10;
redrawFunction();
}

function changeCircuit(x)
{
   // document.GetElementById("imageCircuit").src = "circuit"+x+".jpg";
   var span = document.getElementById('circuitTitle');
    while( span.firstChild ) span.removeChild( span.firstChild );
    
    if(x==1)
    span.appendChild( document.createTextNode("Amplificateur inverseur") );
else if(x==2){ span.appendChild( document.createTextNode("Amplificateur non inverseur") ); document.getElementById("circuitTitle").style.fontSize="25px";}
else if(x==3) span.appendChild( document.createTextNode("Suiveur") );
else if(x==4) span.appendChild( document.createTextNode("Sommateur") );
else if(x==5) span.appendChild( document.createTextNode("Soustracteur") );
    
    document.images['imageCircuit'].src = "img/circuit"+x+".jpg";
    
}